import os
from conans import ConanFile, CMake, tools

class AliceConan(ConanFile):
  name = "alice"
  version = "0.3"
  license = "MIT License"
  author = "Mathias Soeken"
  url = "https://github.com/ruanformigoni/alice"
  description = "Alice is a C++-14 command shell library"
  topics = ("Command-line", "Tool", "C++14", "C++17", "Python")
  exports_sources = "*"
  generators = "cmake_find_package"
  _cmake = None

  def requirements(self):
    self.requires("cli11/1.6.1@bincrafters/stable")
    self.requires("nlohmann_json/3.8.0")
    self.requires("fmt/6.0.0@bincrafters/stable")
    self.requires("pybind11/2.5.0")

  def _patch_cli11(self):
    tools.replace_in_file(
      "include/alice/cli.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )
    tools.replace_in_file(
      "include/alice/command.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )
    tools.replace_in_file(
      "include/alice/validators.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )

  def package(self):
    self._patch_cli11()
    cmake = CMake(self)
    cmake.configure()
    cmake.install()
    tools.rmdir(os.path.join(self.package_folder, "lib", "cmake"))
    self.copy("AliceTools.cmake", dst=os.path.join("lib", "cmake", "alice"), src="tools")
    self.copy("LICENSE", dst="licenses")

  def package_info(self):
    self.cpp_info.components["libalice"].names["cmake_find_package"] = "alice"
    self.cpp_info.components["libalice"].names["cmake_find_package_multi"] = "alice"
    self.cpp_info.components["libalice"].requires = ["fmt::fmt", "cli11::cli11", "nlohmann_json::nlohmann_json"]
    self.cpp_info.components["libalice"].builddirs = [os.path.join("lib", "cmake", "alice")]
    self.cpp_info.components["libalice"].build_modules = [os.path.join(self.cpp_info.components["libalice"].builddirs[0], "AliceTools.cmake")]

    self.cpp_info.components["alice_python"].names["cmake_find_package"] = "alice_python"
    self.cpp_info.components["alice_python"].names["cmake_find_package_multi"] = "alice_python"
    self.cpp_info.components["alice_python"].requires = ["libalice", "pybind11::pybind11"]
    self.cpp_info.components["alice_python"].defines = ["ALICE_PYTHON"]
